import requests
from events.keys import PEXELS_API_KEY as PEX_K, OPEN_WEATHER_API_KEY as OW_K

PEX_URL = 'https://api.pexels.com/v1/search?query='
OW_URL = 'https://api.openweathermap.org/'


def get_location_pic(location):
    loc_url = f'{PEX_URL}{location}&per_page=1&orientation=landscape'
    res = requests.get(loc_url, headers={'authorization': PEX_K})
    # print(res.status_code)
    # print(res)
    return res.json()['photos'][0]['src']['original']


def get_conf_weather(city, state=''):
    coord_url = (
        f'{OW_URL}geo/1.0/direct?q={city}'
        f'{f",{state},US" if state else state}&limit=1&appid={OW_K}'
    )
    coord_res = requests.get(coord_url)
    coord_body = coord_res.json()
    print(coord_body)
    if coord_body == []:
        return {'error': 'this location is misspelled or erroneous'}
    lat, lon = coord_res.json()[0]['lat'], coord_res.json()[0]['lon']

    weath_url = (
        f'{OW_URL}data/2.5/weather?lat={lat}&lon={lon}'
        f'&units=imperial&appid={OW_K}'
    )
    weath_res = requests.get(weath_url)
    weath_body = weath_res.json()

    cur_weather = {
        "temp": f'{weath_body["main"]["temp"]}F*',
        "description": weath_body['weather'][0]['description']
    }
    return cur_weather
